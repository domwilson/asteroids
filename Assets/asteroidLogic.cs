﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class asteroidLogic : MonoBehaviour
{
    public GameObject asteroid;
    public GameObject center;
    public GameObject newAsteroid;
    int spawnCounter;
    int time;
    float angle;
    Vector3 axis = Vector3.zero;
    public Vector3 direction;
    // Use this for initialization
    void Start()
    {
        angle = Mathf.Atan2(this.transform.position.y - center.transform.position.y, this.transform.position.x - center.transform.position.x) * 180 / Mathf.PI;
        direction = center.transform.position + transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        spawnCounter = Random.Range(0, 1625);
        time = (int) Time.time% 16;
        if (time == spawnCounter)
        {
            newAsteroid = Instantiate(asteroid,direction, Quaternion.Slerp(center.transform.rotation, this.transform.rotation, 1.0f), this.transform.parent);
            newAsteroid.GetComponent<Rigidbody2D>().AddForce(-direction * Random.Range(5, 15));
        }
    }
 
}
