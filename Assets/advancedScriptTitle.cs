﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class advancedScriptTitle : MonoBehaviour {
    int counter = 0;
    // Use this for initialization
    void Start()
    {
        StartCoroutine(waiter());
    }

    IEnumerator waiter()
    {
        yield return new WaitForSeconds(0.5f);
        this.GetComponent<Renderer>().enabled = false;
        yield return new WaitForSeconds(0.5f);
        this.GetComponent<Renderer>().enabled = true;
        yield return new WaitForSeconds(0.5f);
        this.GetComponent<Renderer>().enabled = false;
        yield return new WaitForSeconds(0.5f);
        this.GetComponent<Renderer>().enabled = true;
        yield return new WaitForSeconds(0.5f);
        this.GetComponent<Renderer>().enabled = false;
        yield return new WaitForSeconds(0.5f);
        this.GetComponent<Renderer>().enabled = true;
        yield return new WaitForSeconds(0.5f);
        this.GetComponent<Renderer>().enabled = false;
        yield return new WaitForSeconds(0.5f);
        this.GetComponent<Renderer>().enabled = true;
        yield return new WaitForSeconds(0.5f);
        this.GetComponent<Renderer>().enabled = false;


    }

    // Update is called once per frame
    void Update () {


    }
}
