﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shoot : MonoBehaviour {
    public GameObject lazer;
    public GameObject newLazer;
    public GameObject muzzleflash;
    float spawnTime;
    bool cooldown;
    float waittime = 0.2f;
    Quaternion flip;
	// Use this for initialization
	void Start () {
        flip.Set(-1, -1, -1,-1);
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetAxis("Fire1") == 1 && !cooldown)
        {
            newLazer = Instantiate(lazer, this.transform.position, this.transform.rotation);
            newLazer.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.right * 500);
            Instantiate(muzzleflash, this.transform.position, this.transform.rotation);
            this.GetComponent<AudioSource>().Play(0);
            spawnTime = Time.time;
            cooldown = true;
        }
        if (Time.time - spawnTime > waittime)
        {
            cooldown = false;
        }
        if (this.transform.parent.GetComponent<spaceshipMovement>().powerup == true)
        {
            waittime = 0.01f;

        }
        else if (this.transform.parent.GetComponent<spaceshipMovement>().powerup == false)
        {
            waittime = 0.2f;
        }
        if (this.transform.parent.GetComponent<Renderer>().enabled != true)
        {
            cooldown = true;
        }

    }
}
