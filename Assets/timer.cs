﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class timer : MonoBehaviour {
    public GameObject ship;
    float initialTime;
    float newTime;
    
	// Use this for initialization
	void Start () {
        initialTime = Time.time;

    }
	
	// Update is called once per frame
	void Update () {
        newTime = Time.time - initialTime;
        if (!ship.GetComponent<Renderer>().enabled == false)
        {
            float current = Mathf.Round(10 * newTime) / 10;
            if (newTime >= 60.0)
            {
                SceneManager.LoadScene("asteroidsAdvancedLevel", LoadSceneMode.Single);
            }
            this.GetComponent<UnityEngine.UI.Text>().text = current.ToString();
        }
        
    }
}
