﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scoreSystem : MonoBehaviour
{
    GameObject[] asteroids;
    GameObject[] oldasteroids;
    float score = 0;
    // Use this for initialization
    void Start()
    {
        asteroids = new GameObject[] { };
        oldasteroids = new GameObject[] { };
    }

    // Update is called once per frame
    void Update()
    {
        asteroids = GameObject.FindGameObjectsWithTag("asteroid");
        for (int n = 0; n < asteroids.Length; n++)
        {
            bool found = false;
            for (int m = 0; m < oldasteroids.Length; m++)
            {
                if (asteroids[n] == oldasteroids[m])
                {
                    found = true;
                    break;
                }
            }
            if (!found)
            {
              score = score+1f;
            }

        }
        this.GetComponent<UnityEngine.UI.Text>().text = score.ToString();
        oldasteroids = asteroids;
    }
}
