﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class spaceshipMovement : MonoBehaviour {
    public Rigidbody2D rb2d;
    public GameObject killer;
    public GameObject deathscreen;
    float deathTimer = 0;
    Vector3 movement;
    float teleportTimer;
    public bool cooldown = false;
    public bool powerup = false;
    float powerUpTimer;
    // Use this for initialization
    void Start() {
        movement.Set(0f, 0.1f, 0f);
    }

    // Update is called once per frame
    void Update() {
        

        if (Mathf.Abs(Input.GetAxis("Forward")) > 0)
        {
            rb2d.velocity = 8 * transform.right * Input.GetAxis("Forward");
        }
        if (Mathf.Abs(Input.GetAxis("Turn")) > 0)
        {
            transform.Rotate(new Vector3(0, 0, 250 * -Input.GetAxis("Turn")) * Time.deltaTime);
        }
        if(deathTimer != 0 && Time.time-deathTimer >=5)
        {
            SceneManager.LoadScene("asteroidsMainLevel", LoadSceneMode.Single);
        }
        if (Input.GetAxis("Fire2") == 1 && !cooldown)
        {
            teleport(this.transform.position);
            cooldown = true;
            teleportTimer = Time.time;
        }
        if(Time.time - teleportTimer >= 2)
        {
            cooldown = false;
        }
        if(Time.time - powerUpTimer >= 5)
        {
            powerup = false;
        }

    }
    void OnCollisionEnter2D(Collision2D coll)
    {

            if (coll.gameObject.tag == killer.tag)
            {
            this.GetComponent<Renderer>().enabled = false;
            this.GetComponent<Collider2D>().enabled = false;
            this.transform.GetChild(1).gameObject.SetActive(false);
            Instantiate(deathscreen);
            deathTimer = Time.time;
            }

            if(coll.gameObject.tag == "powerup")
        {
            powerup = true;
            powerUpTimer = Time.time;
        }
    }
    void teleport(Vector3 position)
    {
        Vector3 temp = Quaternion.Lerp(transform.rotation, Quaternion.Euler(Vector3.right), Time.deltaTime) * (this.transform.position + movement);
        temp.x = Mathf.Clamp(temp.x, -4, 2);
        temp.y = Mathf.Clamp(temp.y, -9, 9);
        this.transform.position = temp;
    }
}
