﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class killSelf : MonoBehaviour {
    float startTime;
    public GameObject killer;
    public GameObject killer2;
    public bool asteroid;
    private GameObject newAsteroid;
    private GameObject newAsteroid2;
    public GameObject prefab;
    Vector2 RandomPos;
    public float killtime = 12f;
    // Use this for initialization
    void Start() {
        startTime = Time.time;

    }

    // Update is called once per frame
    void Update() {
        if (Time.time - startTime > killtime)
        {
            Destroy(this.gameObject);
            
        }
        RandomPos.Set(Random.Range(-1, 1), Random.Range(-1, 1));
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == killer.tag || collision.gameObject.tag == killer2.tag)
        {
            Destroy(this.gameObject);
            if (asteroid)
            {
                newAsteroid = Instantiate(prefab, this.transform.position + (Vector3)RandomPos, this.transform.rotation );
                foreach (Behaviour behaviour in newAsteroid.GetComponents<Behaviour>())
                {
                    behaviour.enabled = true;
                }
                newAsteroid.GetComponent<Rigidbody2D>().velocity = this.GetComponent<Rigidbody2D>().velocity * RandomPos;
                newAsteroid2 = Instantiate(prefab, this.transform.position - (Vector3)RandomPos, this.transform.rotation);
                foreach (Behaviour behaviour in newAsteroid2.GetComponents<Behaviour>())
                {
                    behaviour.enabled = true;
                }
                newAsteroid2.GetComponent<Rigidbody2D>().velocity = this.GetComponent<Rigidbody2D>().velocity * RandomPos;
            }
            
        }
    }
}
