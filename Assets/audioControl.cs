﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audioControl : MonoBehaviour {
    public GameObject ship;
    public AudioClip death;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (ship.GetComponent<Renderer>().enabled == false)
        {
            this.GetComponent<AudioSource>().clip = death;
        }
    }
}
